"""
Downloads the HuggingFaces dataset and saves it as an artifact
"""
import requests
import tempfile
import os
import mlflow
import click


@click.command(
    help="Downloads the HuggingFaces dataset and saves it as an mlflow artifact "
    "called 'data-csv'."
)
@click.option("--train_url", default="https://huggingface.co/datasets/papluca/language-identification/resolve/main/train.csv")
@click.option("--test_url", default="https://huggingface.co/datasets/papluca/language-identification/resolve/main/test.csv")
@click.option("--val_url", default="https://huggingface.co/datasets/papluca/language-identification/resolve/main/valid.csv")
def load_raw_data(train_url, test_url, val_url):
    with mlflow.start_run() as mlrun:
        local_dir = tempfile.mkdtemp()
        #local_dir = "data/"
        #os.makedirs(local_dir, exist_ok=True)
        for split, split_url in zip(["train.csv", "valid.csv", "test.csv"], 
                                    [train_url, val_url, test_url]):
            local_filename = os.path.join(local_dir, split)
            print(f"Downloading {split_url} to {local_filename}")
            r = requests.get(split_url, stream=True)
            with open(local_filename, "wb") as f:
                for chunk in r.iter_content(chunk_size=1024):
                    if chunk:  # filter out keep-alive new chunks
                        f.write(chunk)
            
        print(f"Uploading artifacts")
        mlflow.log_artifacts(local_dir, artifact_path="dataset")


if __name__ == "__main__":
    load_raw_data()