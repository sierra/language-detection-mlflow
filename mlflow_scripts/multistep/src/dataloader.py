import pandas as pd
import tensorflow as tf

from sklearn import preprocessing
from tensorflow.keras import layers

AUTOTUNE = tf.data.AUTOTUNE

class DataLoader():
    def __init__(self, lang_list, data_directory):
        self.raw_dict = dict()
        self.df_dict = dict()
        self.ds_dict = dict()
        self.lang_list = lang_list
        self.num_classes = len(lang_list)
        #self.label_encoder = None
        self.vectorize_layer = None
        self.load_data(data_directory)
    
    def load_data(self, data_directory):
        self.df_dict["train"] = pd.read_csv(f"{data_directory}/train.csv")
        self.df_dict["val"] = pd.read_csv(f"{data_directory}/valid.csv")
        self.df_dict["test"] = pd.read_csv(f"{data_directory}/test.csv")
        if self.lang_list is not None:
            for key_df in self.df_dict.keys():
                self.df_dict[key_df] = self.df_dict[key_df].loc[self.df_dict[key_df].labels.isin(self.lang_list)]

    def _build_label_encoder(self):
        self.lang2lab = {lang:lab for lab, lang in enumerate(sorted(self.lang_list))}
        self.lab2lang = {lab:lang for lab, lang in enumerate(sorted(self.lang_list))}

    def _build_ds(self):
        for key_ds in self.df_dict.keys():
            labels = tf.keras.utils.to_categorical([self.lang2lab[elem] for elem in self.df_dict[key_ds].pop('labels')], num_classes=self.num_classes)
            #labels = tf.keras.utils.to_categorical(self.label_encoder.transform(self.df_dict[key_ds].pop('labels')), num_classes=self.num_classes)
            self.raw_dict[key_ds] = tf.data.Dataset.from_tensor_slices((self.df_dict[key_ds]["text"].to_list(), labels)) # X, y

    def _create_vectorize_layer(self, max_features, sequence_length):
        self.vectorize_layer = layers.TextVectorization(
            standardize="lower_and_strip_punctuation",
            max_tokens=max_features,
            output_mode='int',
            output_sequence_length=sequence_length)
        self.vectorize_layer.adapt(self.df_dict["train"]["text"].to_list())
    
    def _apply_vectorized_layer(self):
        for key_ds in self.raw_dict.keys():
            self.ds_dict[key_ds] = self.raw_dict[key_ds].map(lambda x, y: (self.vectorize_layer(x), y))

    def _prepare_batched_data(self, batch_size):
        for key_ds in self.ds_dict.keys():
            self.ds_dict[key_ds] = self.ds_dict[key_ds].batch(batch_size=batch_size)
            self.ds_dict[key_ds] = self.ds_dict[key_ds].prefetch(AUTOTUNE)

    def create_dataset(self, max_features, sequence_length, batch_size):
        """ Creates tf data dataset. It uses the pandas dataframes contained in
        df_dict and transforms them into usable tf.data datasets. 

        Args:
            max_features (int): Maximum number of tokens for building the neural network
            sequence_length (int): Maximum size for each input sentence
            batch_size (int): Size for batching each dataset 

        Returns:
            train_ds (tf.data.Dataset): Training dataset ready to use
            val_ds (tf.data.Dataset): Validation dataset ready to use
            test_ds (tf.data.Dataset): Test dataset ready to use
        """
        self._build_label_encoder()
        self._build_ds()
        self._create_vectorize_layer(max_features, sequence_length)
        self._apply_vectorized_layer()
        self._prepare_batched_data(batch_size)
        return self.ds_dict["train"], self.ds_dict["val"], self.ds_dict["test"]
