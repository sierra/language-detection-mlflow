import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras import losses


def get_simple_model(max_features=10000, embedding_dim=16):
    model = tf.keras.Sequential([
        layers.Embedding(max_features + 1, embedding_dim),
        layers.Dropout(0.2),
        layers.GlobalAveragePooling1D(),
        layers.Dropout(0.2),
        layers.Dense(3)])
    return model

def get_end2end(vectorize_layer, model):
    export_model = tf.keras.Sequential([
        vectorize_layer,
        model,
        layers.Activation('softmax')])
    return export_model