import argparse
import joblib
import os
import mlflow
import requests
import tensorflow as tf

from tensorflow.keras import losses
from urllib.parse import urlparse
from src.dataloader import DataLoader
from src.models import get_simple_model, get_end2end
from tensorflow.python.saved_model import signature_constants
import mlflow.tensorflow


def parse_args():
    parser = argparse.ArgumentParser(description="Train a simple neural network for language identification")
    parser.add_argument("data_dir", type=str)
    parser.add_argument("--train_url", type=str)
    parser.add_argument("--test_url", type=str)
    parser.add_argument("--val_url", type=str)
    parser.add_argument("--max_features", type=int)
    parser.add_argument("--sequence_length", type=int)
    parser.add_argument("--batch_size", type=int)
    parser.add_argument("--embedding_dim", type=int)
    parser.add_argument("--epochs", type=int)
    args = parser.parse_args()
    return args

def load_raw_data(local_dir, train_url, test_url, val_url):
    #local_dir = "data/"
    os.makedirs(local_dir, exist_ok=True)
    for split, split_url in zip(["train.csv", "valid.csv", "test.csv"], 
                                [train_url, val_url, test_url]):
        local_filename = os.path.join(local_dir, split)
        print(f"Downloading {split_url} to {local_filename}")
        r = requests.get(split_url, stream=True)
        with open(local_filename, "wb") as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)

tag=[tf.compat.v1.saved_model.tag_constants.SERVING]
key=signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY

# Setting an experiment name to group experiments
experiment_name = 'exercise-single-step'
mlflow.set_experiment(experiment_name)
experiment = mlflow.get_experiment_by_name(experiment_name)
client = mlflow.tracking.MlflowClient()
run = client.create_run(experiment.experiment_id)
mlflow.tensorflow.autolog()

def main():
    with mlflow.start_run(run_id = run.info.run_id): # If no experiment is specified, this method can be called without arguments
        args = parse_args()
        load_raw_data(args.data_dir, args.train_url, args.test_url, args.val_url)
        lang_list = ["es", "en", "de"]
        ds_loader = DataLoader(lang_list, args.data_dir)
        train_ds, val_ds, test_ds = ds_loader.create_dataset(args.max_features, 
                                                            args.sequence_length, 
                                                            args.batch_size)
        model = get_simple_model(args.max_features, args.embedding_dim)
        model.compile(loss=losses.CategoricalCrossentropy(from_logits=True, 
                                                        label_smoothing=0.1),
                    optimizer='adam',
                    metrics=['accuracy'])
        model.fit(
            train_ds,
            validation_data=val_ds,
            epochs=args.epochs)
        loss, accuracy = model.evaluate(test_ds)

        print("Test loss: ", loss)
        print("Test accuracy: ", accuracy)

        mlflow.log_metric("loss", loss)
        mlflow.log_metric("accuracy", accuracy)
        
        model_path = "saved_model"
        export_model = get_end2end(ds_loader.vectorize_layer, model)
        saved_path = os.path.join(model_path, "model")
        export_model.save(saved_path)
        mlflow.tensorflow.log_model(tf_saved_model_dir=saved_path,
                                    tf_meta_graph_tags=tag,
                                    tf_signature_def_key=key,
                                    artifact_path="tf-models",
                                    registered_model_name="exercise-language-model")


if __name__ == "__main__":
    main()