# MLFlow

In this repository you will find all the required code for running the language detection exercise in mlflow. 

## Requirements

1. Install `mlflow` using `pip`
2. Define the following environment variables accordingly:
  ```
  MLFLOW_TRACKING_URI
  MLFLOW_TRACKING_PASSWORD 
  MLFLOW_TRACKING_USERNAME
  ```

## Single step

`mlflow_scripts/single_step`

For the single step execution, we have defined a `Dockerfile` for the requirements. You need to execute the following steps:

1. Create a Dockerfile (In this case, you can just review the one we provide)
2. Build the corresponding image using docker build. For instance: `docker build -t mlflow-language-detection -f Dockerfile .`
3. Execute the desired entrypoint:
  * `mlflow run mlflow_scripts/single_step`

## Multistep

1. Execute the desired entrypoint:
  * `mlflow run mlflow_scripts/multistep`

**Note**: You will notice a duplicated folder (`src`) inside both `mlflow_scripts` folders. This was done because in the single step example, mlflow copies the project code (only the contents of the `mlflow_scripts/single_step`) inside the docker image, then we had to duplicate the src modules for the sake of simplicity. In the case of a single project in your repository, there should not be duplicates.