FROM condaforge/miniforge3

RUN pip install mlflow>=1.0 \
    && pip install tensorflow==2.8.0 \
    && pip install pandas==1.3.5 \
    && pip install numpy==1.21.5 \
    && pip install scikit-learn==1.0.2 \
    && pip install joblib \
    && pip install cloudpickle \
    && pip install protobuf==3.20.1